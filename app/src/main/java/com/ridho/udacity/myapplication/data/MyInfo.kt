package com.ridho.udacity.myapplication.data

data class MyInfo(
    var name: String = "",
    var nickname: String = ""
)